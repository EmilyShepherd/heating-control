package main

import (
	"fmt"
)

type Configuration struct {
	Kind           string                        `yaml:"kind"`
	ApiVersion     string                        `yaml:"apiVersion"`
	Queue          QueueConfiguration            `yaml:"queue"`
	CentralHeating []CentralHeatingConfiguration `yaml:"centralHeatingLoops"`
	HotWater       []HotWaterConfiguration       `yaml:"hotWaterLoops"`
}

type QueueConfiguration struct {
	Host      string `yaml:"host"`
	Port      int    `yaml:"port"`
	BaseTopic string `yaml:"baseTopic"`
}

type CentralHeatingConfiguration struct {
	GroupName string              `yaml:"groupName,omitempty"`
	Switch    SwitchConfiguration `yaml:"switch"`
}

type SwitchConfiguration struct {
	Name      string `yaml:"name"`
	Attribute string `yaml:"attribute"`
}

func (s SwitchConfiguration) Topic(base string) string {
	return fmt.Sprintf("%s/%s/set/%s", base, s.Name, s.Attribute)
}

type HotWaterConfiguration struct {
	WithCentralHeating  bool                        `yaml:"withCentralHeating"`
	Switch              SwitchConfiguration         `yaml:"switch"`
	CentralHeatingLoops []string                    `yaml:"centralHeatingLoops,omitempty"`
	DesiredRuntime      DesiredRuntimeConfiguration `yaml:"desiredRuntime"`
	Schedule            []Schedule                  `yaml:"schedule"`
}

type DesiredRuntimeConfiguration struct {
	ActiveDuration string `yaml:"activeDuration"`
	Window         string `yaml:"window"`
}

type ScheduleType string

const (
	TypeRequired ScheduleType = "Required"
	TypeOptional              = "Optional"
)

type Schedule struct {
	Type      ScheduleType `yaml:"type"`
	StartTime string       `yaml:"startTime"`
	EndTime   string       `yaml:"endTime"`
}
