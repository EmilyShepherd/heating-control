package main

import (
	"encoding/json"
	"fmt"
	"os"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gopkg.in/yaml.v3"
)

var config Configuration
var client mqtt.Client

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connect lost: %v", err)
}

func main() {
	data, _ := os.ReadFile("/tmp/config.yaml")
	yaml.Unmarshal(data, &config)

	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%d", config.Queue.Host, config.Queue.Port))
	opts.SetClientID("go_mqtt_client")
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler
	client = mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	end := make(chan int)

	sub(client)

	<-end
	client.Disconnect(250)
}

func runWithHeating(hwLoop HotWaterConfiguration, chLoop CentralHeatingConfiguration) bool {
	if !hwLoop.WithCentralHeating {
		return false
	}
	if len(hwLoop.CentralHeatingLoops) == 0 {
		return true
	}
	for _, chLoopName := range hwLoop.CentralHeatingLoops {
		if chLoopName == chLoop.GroupName {
			return true
		}
	}
	return false
}

func switchBoolToString(on bool) string {
	if on {
		return "ON"
	} else {
		return "OFF"
	}
}

func switchSet(sw SwitchConfiguration, on bool) {
	setting := switchBoolToString(on)

	fmt.Printf("Setting %s.%s to %s\n", sw.Name, sw.Attribute, setting)
	client.Publish(sw.Topic(config.Queue.BaseTopic), 0, false, setting)
}

func checkHeatingLoops(chLoop CentralHeatingConfiguration, on bool) {
	for _, hwLoop := range config.HotWater {
		if !runWithHeating(hwLoop, chLoop) {
			continue
		}
		switchSet(hwLoop.Switch, on)

	}
}

func sub(client mqtt.Client) {
	devicesTopic := fmt.Sprintf("%s/bridge/devices", config.Queue.BaseTopic)
	groupsTopic := fmt.Sprintf("%s/bridge/groups", config.Queue.BaseTopic)
	devices := make(map[string]DeviceInfo)
	groups := make(map[string]GroupInfo)

	deviceUpdateHandler := func(client mqtt.Client, msg mqtt.Message) {
		if len(devices) == 0 || len(groups) == 0 {
			return
		}
		for _, chLoop := range config.CentralHeating {
			valves := make(map[string]bool)
			for _, deviceLookup := range groups[chLoop.GroupName].Members {
				device := devices[deviceLookup.Id]
				fmt.Printf("Subscribing to %s\n", device.Name)
				deviceTopic := fmt.Sprintf("%s/%s", config.Queue.BaseTopic, device.Name)
				client.Subscribe(deviceTopic, 0, func(client mqtt.Client, msg mqtt.Message) {
					var valve RadiatorValve
					json.Unmarshal(msg.Payload(), &valve)

					valves[msg.Topic()] = valve.RequiresHeat()

					for _, active := range valves {
						if active {
							fmt.Printf("A radiator requires heat\n")
							switchSet(chLoop.Switch, true)
							checkHeatingLoops(chLoop, true)
							return
						}
					}

					fmt.Printf("No radiators require heat\n")
					switchSet(chLoop.Switch, false)
					checkHeatingLoops(chLoop, false)
				})
				// No-op to cause the state to be updated for the subscriber above
				client.Publish(fmt.Sprintf("%s/set", deviceTopic), 0, false, `{"valve_detection":""}`)
			}
		}
	}

	client.Subscribe(devicesTopic, 0, func(client mqtt.Client, msg mqtt.Message) {
		var deviceList []DeviceInfo
		json.Unmarshal(msg.Payload(), &deviceList)
		for _, device := range deviceList {
			devices[device.Id] = device
		}
		deviceUpdateHandler(client, msg)
	})
	client.Subscribe(groupsTopic, 0, func(client mqtt.Client, msg mqtt.Message) {
		var groupList []GroupInfo
		json.Unmarshal(msg.Payload(), &groupList)
		for _, chLoop := range config.CentralHeating {
			for _, group := range groupList {
				if group.Name == chLoop.GroupName {
					groups[group.Name] = group
				}
			}
		}
		deviceUpdateHandler(client, msg)
	})
}
