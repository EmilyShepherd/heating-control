package main

type DeviceInfo struct {
	Id    string `json:"ieee_address"`
	Name  string `json:"friendly_name"`
	Model string `json:"model_id"`
}

type GroupInfo struct {
	Name    string       `json:"friendly_name"`
	Members []DeviceInfo `json:"members"`
}

type ValveState string

const (
	ValveStateHeating ValveState = "heat"
	ValveStateIdle               = "idle"
)

type RadiatorValve struct {
	State ValveState `json:"running_state"`
}

func (r RadiatorValve) RequiresHeat() bool {
	return r.State == ValveStateHeating
}
